package com.lxm.idgenerator.handler;

import com.lxm.idgenerator.bean.IdMeta;
import com.lxm.idgenerator.service.bean.Id;
import com.lxm.idgenerator.util.BitUtil;

/**
 * @author luoxiaomin
 * @version 1.0.0
 * @date 2018/6/25
 * @time 14:02
 */
public abstract class BaseHandler implements IdHandler {


    protected long sequence = Id.INIT_SEQUEUE;
    protected long lastTimestamp = Id.INIT_TIMESTAMP;

    public BaseHandler() {
        super();
    }

    public long handle(TimeHandler timer, Id id, IdMeta meta) {
        long timestamp = timer.getTime();
        timer.validateClockBackward(lastTimestamp, timestamp);

        // 如果在同一时间单位，则序列号自增,同时判断是否超过阈值, 假如超过阈值则暂停直至下一秒
        if (timestamp == lastTimestamp) {
            sequence = (sequence + 1) & meta.getSeqBitsMask();
            if (sequence == Id.INIT_SEQUEUE) {
                timestamp = timer.nextTime(lastTimestamp);
            }
        } else {
            // 新的时间范围，序列号重置
            sequence = Id.INIT_SEQUEUE;
        }
        lastTimestamp = timestamp;
        id.setSeq(sequence);
        id.setTime(timestamp);
        return BitUtil.id2long(id, meta);
    }

}
